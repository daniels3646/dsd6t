import d6tflow
import csv
import pandas as pd
import numpy as np
from sklearn import preprocessing
import os
import time
import copy
import sys
from functools import partial
from sklearn.metrics import log_loss,auc,roc_curve
from sklearn.metrics import r2_score
from sklearn.base import BaseEstimator, TransformerMixin, ClassifierMixin
from sklearn.utils import check_array
import dask.dataframe as dd
from dask.diagnostics import ProgressBar
from dask_ml.preprocessing import DummyEncoder
from datetime import date
from dask_ml.model_selection import train_test_split
import dask
from dask.delayed import delayed ,Delayed
from .target import TaskParquetDask
from .viz_util import basic_info_df
sys.path.insert(0, "/home/daniel/Documents/DataScience/kaggleKernels/heuristics/dsd6t")
import dskaggle
import json
import os
from pathlib import Path

# def load_config(profile,key,filename = None):
  # if not filename:
    # home = str(Path.home())
    # filename = os.path.join(home,".config/dskaggleConfig.json")
    # if not os.path.isfile(filename):
      # raise Exception(f"If no 'filename' paramater specifed, assume '.dskaggleConfig.json' exists at HOME: {home}")

  # with open(filename) as f:
    # data = json.load(f)
    # if profile not in data:
      # raise Exception(f"Undefined profile '{profile}' in file '{filename}'")
    # return data[profile][key]

# current_proj = 'instacart' # to delete

# dtypes = dskaggle.load_config(current_proj,'DTYPES')

# dtypes_convert = {
    # "np.str" : np.str,
    # "np.int8" : np.int8,
    # "np.int16" : np.int16,
    # "np.int32" : np.int32,
    # "np.int64" : np.int64,
    # "np.uint8" : np.uint8,
    # "np.uint16" : np.uint16,
    # "np.uint32" : np.uint32,
    # "np.uint64" : np.uint64,
    # "np.intp" : np.intp,
    # "np.uintp" : np.uintp,
    # "np.float32" : np.float32,
    # "category" : 'category'
  # }

dsts = ['train','test']

# Nicely formatted time string
def hms_string(sec_elapsed):
  h = int(sec_elapsed / (60 * 60))
  m = int((sec_elapsed % (60 * 60)) / 60)
  s = sec_elapsed % 60
  return "{}:{:>02}:{:>05.2f}".format(h, m, s)

# Regression chart, we will see more of this chart in the next class.
def chart_regression(pred, y):
  t = pd.DataFrame({'pred': pred, 'y': y.flatten()})
  t.sort_values(by=['y'], inplace=True)
  a = plt.plot(t['y'].tolist(), label='expected')
  b = plt.plot(t['pred'].tolist(), label='prediction')
  plt.ylabel('output')
  plt.legend()
  plt.show()

def get_cols(file_path,project_settings):
  folder_path = project_settings['PATH']
  full_path = os.path.join(folder_path , file_path )
  # print(full_path)
  with open(full_path, 'r') as f:
    d_reader = csv.DictReader(f)
    cols = d_reader.fieldnames
  return cols

# class readTrainCols(TaskParquetDask):
  # persist = get_cols('input/train.csv')
  # def run(self):
    # file_path = 'input/train.csv'
    # cols = get_cols(file_path)
    # # self.persist = cols
    # print(cols)
    # dict_of_series = dict(zip(cols,[dd.read_csv('../' + file_path,usecols=[col],dtype={col : dtypes_convert[dtypes[col]]}) for col in cols]))
    # self.save(dict_of_series)

# class readTestCols(TaskParquetDask):
  # persist = get_cols('input/test.csv')
  # def run(self):
    # file_path = 'input/test.csv'
    # cols = get_cols(file_path)
    # # self.persist = cols
    # print(cols)
    # dict_of_series = dict(zip(cols,[dd.read_csv('../' + file_path,usecols=[col],dtype={col : dtypes_convert[dtypes[col]]}) for col in cols]))
    # self.save(dict_of_series)

def oneHot(client,list_cols,new_name,func):
  df = dd.concat(list_cols,axis=1)
  print('df columns', df.columns)
  de = DummyEncoder()
  new_df = de.fit_transform(df.to_frame().categorize(columns=df.columns[0]))
  return new_df

def applymapOnColumns(df,new_names,func):
  new_df = df.applymap(func).rename(columns=dict(zip(df.columns,new_names)))
  return new_df

def applyOnColumns(df,new_name,func,meta):
  print('df columns', df.columns)
  new_df = df.apply(lambda x: func(*list(map(lambda col: x[col],df.columns))),axis=1,meta=meta).rename(new_name).to_frame()
  return new_df

def functionOnColumns(client,list_cols,new_names,func):
  df = dd.concat(list_cols,axis=1)
  print('df columns', df.columns)
  new_df = func(df).rename(columns=dict(zip(df.columns,new_names)))
  return new_df

def group_idx_value(groupby,index,value): # groupby - cols , index - rows , value - values
  df_concat = dd.concat([groupby,index,value],axis=1)
  final = pd.DataFrame()
  for name , group in df_concat.groupby(groupby.columns[0]):
    if final.empty:
      final = group.set_index(index.columns[0])[[value.columns[0]]].rename(columns={value.columns[0]:name})
    else:
      final = final.join(group.set_index(index.columns[0])[[value.columns[0]]].rename(columns={value.columns[0]:name}))
  print(final.head())
  return final

class StackingEstimator(BaseEstimator, TransformerMixin):
  def __init__(self, estimator):
    self.estimator = estimator

  def fit(self, X, y=None, **fit_params):
    self.estimator.fit(X, y, **fit_params)
    return self

  def transform(self, X):
    X = check_array(X)
    X_transformed = np.copy(X)
    # add class probabilities as a synthetic feature
    if issubclass(self.estimator.__class__, ClassifierMixin) and hasattr(self.estimator, 'predict_proba'):
      X_transformed = np.hstack((self.estimator.predict_proba(X), X))

    # add class prodiction as a synthetic feature
    X_transformed = np.hstack((np.reshape(self.estimator.predict(X), (-1, 1)), X_transformed))

    return X_transformed

