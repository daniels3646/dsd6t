import matplotlib.pyplot as plt
from dask.distributed import Client
from scipy.stats import skew
import pandas as pd
import numpy as np
from sklearn.metrics import roc_auc_score
from featexp import univariate_plotter
from featexp import get_trend_stats
from featexp import get_univariate_plots
from dask_ml.model_selection import train_test_split
import os
import dask.dataframe as dd
from dask.array.stats import skew
from dask.array import argtopk
import dask.array as da
import datetime as dt
import seaborn as sns
from tabulate import tabulate
import d6tflow
from dask.delayed import delayed ,Delayed
import pandas_profiling

def get_val(train_col_list,target):
  df = dd.from_pandas(pd.concat(train_col_list,axis=1),npartitions = partitions)
  print('train_params: ',train_params.head())
  print('train_target: ',train_target.head())
  X_train, X_test, Y_train, Y_test = train_test_split(df,target, test_size=0.30, random_state=100)
  print('X_train length: ' , len(X_train))
  print('Y_train length: ' , len(Y_train))
  print('X_test length: ' , len(X_test))
  print('Y_test length: ' , len(Y_test))
  new_train = dd.concat([X_train,Y_train],axis=1).compute()
  new_val = dd.concat([X_test,Y_test],axis=1).compute()
  return (new_train , new_val)

def featexp_univariate(train_col_list,target):
  (new_train , new_val) = get_val(train_col_list,target)
  get_univariate_plots(data=new_train, target_col=target.columns[0], features_list=[series.columns[0] for series in train_col_list], data_test=new_val)

def featexp_trend_stats(train_col_list,target):
  (new_train , new_val) = get_val(train_col_list,target)
  stats = get_trend_stats(data=new_train, target_col=target.columns[0], data_test=new_val)
  print(stats)

def concat_outs(inpt,dask=False):
  if dask:
    return dd.concat([x.load(dask=True) for x in list(inpt.values())],axis=1)
  else:
    return pd.concat([x.load() for x in list(inpt.values())],axis=1)

def basic_info(task,cols=None,html=False):
  class basic_info(d6tflow.tasks.TaskData):
    def requires(self):
      return task()
    def run(self):
      if cols:
        for col in cols:
          print('------------------{}------------------'.format(col))
          df = self.input()[col].load()
          self.actual_info(df,html)
      else:
        if isinstance(self.input(),dict):
          list_of_dfs = [x.load() for x in list(self.input().values())]
          df_concat = pd.concat(list_of_dfs,axis=1)
        else:
          df_concat = self.input().load()
        self.actual_info(df_concat,html)
    def actual_info(self,df,html=False):
      if not isinstance(df, pd.Series) and not isinstance(df, pd.DataFrame):
        df = df.compute()
      if html:
        html = df.to_html(max_rows=10000)
        #write html to file
        text_file = open("output.html", "w")
        text_file.write(html)
        text_file.close()
      else:
        print('columns: ' ,df.columns , 'num of columns: ', len(df.columns))
        print('index names: ', df.index.names)
        print('num of rows: ', len(df))
        print('dtypes: ', df.dtypes)
        print('head: ', prttPrnt(df.head(30)))
        print('tail: ', prttPrnt(df.tail(10)))
  return basic_info()

def see_outputs(task):
  class see_outputs(d6tflow.tasks.TaskData):
    def requires(self):
      return task()
    def run(self):
      print('outputs: ')
      list(map(lambda x: print('  ',x),self.input().keys()))
  return see_outputs()

def basic_info_df(df,cols=None,pd=True):
  print(type(df))
  if cols:
    df = df[cols]
  else:
    df = df
  if not pd:
    df = df.compute()
  print('columns: ' ,df.columns , 'num of columns: ', len(df.columns))
  print('index names: ', df.index.names)
  print('num of rows: ', len(df))
  print('dtypes: ', df.dtypes)
  print('head: ', prttPrnt(df.head(30)))
  print('tail: ', prttPrnt(df.tail(10)))

def extensive_info(task,cols=None):
  class basic_info(d6tflow.tasks.TaskData):
    def requires(self):
      return task()
    def run(self):
      if cols:
        for col in cols:
          df = self.input()[col].load(pd=True).compute()
          self.actual_info(df)
      else:
        if isinstance(self.input(),dict):
          list_of_dfs = [x.load(pd=True) for x in list(self.input().values())]
          df_concat = pd.concat(list_of_dfs,axis=1).compute()
        else:
          df_concat = self.input().load(pd=True).compute()
        self.actual_info(df_concat)
    def actual_info(self,df):
      profile = df.profile_report(title='Pandas Profiling Report')
      profile.to_file(output_file="output.html")
  return basic_info()

def sanity_check(inpt,dfcols,merged=False,pd=True):
  df = df_concat(inpt,dfcols,merged,pd)
  print('columns: ' ,df.columns , 'num of columns: ', len(df.columns))
  # print('index names: ', df.index.names)
  print('num of rows: ', len(df))
  print('dtypes: ', df.dtypes)
  print('head: ', prttPrnt(df.head(30)))
  print('tail: ', prttPrnt(df.tail(10)))

def basic_info_ser(ser,cols=None,pd=True):
  print(type(ser))
  if cols:
    ser = ser[cols]
  else:
    ser = ser
  if not pd:
    ser = ser.compute()
  print('name: ' ,ser.name)
  print('index names: ', ser.index.names)
  print('num of rows: ', len(ser))
  print('dtypes: ', ser.dtypes)
  # print('head: ', prttPrnt(ser.head(30)))
  # print('tail: ', prttPrnt(ser.tail(10)))
  print('head: ' , ser.head(30))
  print('tail: ' , ser.tail(10))

def prttPrnt(df):
  print(tabulate(df, headers='keys', tablefmt='psql'))

def missing_values(df):
  total = df.isnull().sum().sort_values(ascending = False)
  percent = (df.isnull().sum()/df.isnull().count()*100).sort_values(ascending = False)
  missing__train_data  = pd.concat([total, percent], axis=1, keys=['Total', 'Percent'])
  print(missing__train_data)

def skewed_feats(train,test):

  numeric_feats_train = train.select_dtypes(exclude=['object'])
  print('train')
  for col in numeric_feats_train.columns:
    print('col: {}, skew: {}'.format(col,skew(numeric_feats_train[col]).compute()))

  numeric_feats_test = test.select_dtypes(exclude=['object'])
  print('test')
  for col in numeric_feats_test.columns:
    print('col: {}, skew: {}'.format(col,skew(numeric_feats_test[col]).compute()))

def freq(inpt):
  reorder_dow_freq = inpt.value_counts()
  fig = plt.figure()
  reorder_dow_freq.plot(kind='bar', color='skyblue')
  plt.title('Frequency')
  plt.show()

def bar(inpt):
  # fig = plt.Figure()
  plt.bar(inpt.index, inpt ,color='lightpink')
  plt.xticks(rotation=90)
  plt.xlabel(inpt.name)
  plt.title('bar plot')
  return plt

def stem(inpt):
  inpt.iloc[:,0].compute().plot(title='stem of: {}'.format(col))
  plt.show()

def wrt(inpt,y,wrt,merged=False): # y and wrt should be strings
  client = Client()
  concat = df_concat(inpt,[y,wrt],merged)
  df_grouped = client.compute(concat.groupby(wrt)[y].sum()).result()
  client.close()
  plt.plot(df_grouped)
  plt.title('{} wrt {}'.format(y,wrt))
  plt.legend(loc=0)
  plt.ylabel('sum of {}'.format(y))
  plt.xlabel(wrt)
  plt.show()

def hist(inpt,col,bins,min_lim,max_lim,merged=False):
  concat = df_concat(inpt,col,merged).iloc[:,0].compute()
  concat.plot(kind='hist',bins=bins,figsize=(15, 5),title='histogram of: {}'.format(col), facecolor='navajowhite', edgecolor='orange')
  plt.xlim((min_lim,max_lim))
  plt.show()

def dist_plot(inpt1,inpt2,train_col,test_col,merged=[False,False]):
  # plot dist curves for train and test data for the given column name
  concat_train = df_concat(inpt1,train_col,merged[0]).compute()
  concat_test = df_concat(inpt2,test_col,merged[1]).compute()

  fig, ax = plt.subplots(figsize=(10, 10))
  sns.distplot(concat_train.dropna(), color='green', ax=ax).set_title(train_col, fontsize=16)
  sns.distplot(concat_test.dropna(), color='purple', ax=ax).set_title(test_col, fontsize=16)
  plt.xlabel(train_col, fontsize=15)
  plt.legend(['train', 'test'])
  plt.show()

def pie(inpt,col,merged=False):
  client = Client()
  concat = client.compute(delayed(df_concat)(inpt,col,merged).iloc[:,0]).result()
  client.close()
  plt.figure(figsize=(10,10))
  labels = (np.array(concat.index))
  sizes = (np.array((concat / concat.sum())*100))
  plt.pie(sizes, labels=labels,
          autopct='%1.1f%%', startangle=200)
  plt.title("Departments distribution", fontsize=15)
  plt.show()

def correlations(list_cols,specific_col=None):
  df_concat = dd.concat(list_cols,axis=1).compute()
  if specific_col:
    corrs = df_concat.corr()[specific_col].sort_values()
    # Display correlations
    print('Most Positive Correlations:\n', corrs.tail(15))
    print('\nMost Negative Correlations:\n', corrs.head(15))
  else:
    corrs = df_concat.corr()
    # Heatmap of correlations
    sns.heatmap(corrs, cmap = plt.cm.RdYlBu_r, vmin = -0.25, annot = True, vmax = 0.6)
    plt.title('Correlation Heatmap');
    plt.show()

def missmap(list_cols): # timestamp , cols , target , group_param(graph num)
  print(list_cols[0].columns[0])
  df_concat = dd.concat(list_cols,axis=1).set_index(list_cols[0].columns[0]).compute()
  print('df_concat.columns',df_concat.columns)
  # print(df_concat.iloc[[999999]])
  # Plot missing values per building/meter
  f,a=plt.subplots(1,4)
  for meter in np.arange(len(df_concat[df_concat.columns[2]].unique())):
    df = df_concat[df_concat[df_concat.columns[2]]==meter].copy().reset_index()
    print('df.columns',df.columns)
    df[df_concat.index.name] = pd.to_datetime(df[df_concat.index.name])
    # df[df_concat.index.name] = df[df_concat.index.name].astype(int)
    df[df_concat.index.name] -= df[df_concat.index.name].min()
    df[df_concat.index.name] = (df[df_concat.index.name].dt.total_seconds() / 3600).astype(int)
    # print(len(df_concat[df_concat.columns[0]].unique()))
    missmap = np.empty((len(df_concat[df_concat.columns[0]].unique()), df[df_concat.index.name].max()+1))
    missmap.fill(np.nan)
    for l in df.values:
      if l[3]!=meter:continue
      missmap[int(l[1]), int(l[0])] = 0 if l[2]==0 else 1
    a[meter].set_title(f'meter {meter:d}')
    sns.heatmap(missmap, cmap='Paired', ax=a[meter], cbar=False)
  plt.show()
  # Legend:
  # * X axis: hours elapsed since Jan 1st 2016, for each of the 4 meter types
  # * Y axis: building_id
  # * Brown: meter reading available with non-zero value
  # * Light blue: meter reading available with zero value
  # * White: missing meter reading

