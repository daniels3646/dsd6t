import os
import csv
import matplotlib.pyplot as plt
import d6tflow
import luigi
from luigi.util import inherits
import sklearn, sklearn.datasets, sklearn.svm, sklearn.linear_model
import pandas as pd
import dask.dataframe as dd
from dask.diagnostics import ProgressBar
from dask_ml.preprocessing import DummyEncoder
from datetime import date
from dask_ml.model_selection import train_test_split
from functools import partial
from dask.delayed import delayed ,Delayed
from dask.distributed import Client
import numpy as np
import dask
import sys
import time
sys.path.insert(0, "/home/daniel/Documents/DataScience/kaggleKernels/heuristics/dsd6t")
import dskaggle
from dskaggle import util,viz_util
from dskaggle.target import TaskParquetDask
from dskaggle.viz_util import prttPrnt,basic_info,basic_info_df,basic_info_ser,extensive_info,df_concat
import argparse
import inspect
import lightgbm as lgb
from sklearn.ensemble import GradientBoostingClassifier
from sklearn import metrics

do_preprocess = True

######################################################################################### LOADING

aisles = 'input/aisles.csv'
departments = 'input/departments.csv'
order_products_prior = 'input/order_products_prior.csv'
order_products_train = 'input/order_products_train.csv'
orders = 'input/orders.csv'
products = 'input/products.csv'

class readAisles(TaskParquetDask):
  persist = util.get_cols(aisles)
  def run(self):
    file_path = aisles
    cols = util.get_cols(file_path)
    # self.persist = cols
    print(cols)
    dict_of_series = dict(zip(cols,[dd.read_csv('../' + file_path,usecols=[col]) for col in cols]))
    self.save(dict_of_series)

class readDepartments(TaskParquetDask):
  persist = util.get_cols(departments)
  def run(self):
    file_path = departments
    cols = util.get_cols(file_path)
    # self.persist = cols
    print(cols)
    dict_of_series = dict(zip(cols,[dd.read_csv('../' + file_path,usecols=[col]) for col in cols]))
    self.save(dict_of_series)

class readOrderproductsprior(TaskParquetDask):
  persist = util.get_cols(order_products_prior)
  def run(self):
    file_path = order_products_prior
    cols = util.get_cols(file_path)
    # self.persist = cols
    print(cols)
    dict_of_series = dict(zip(cols,[dd.read_csv('../' + file_path,usecols=[col],dtype={col : util.dtypes_convert[util.dtypes[col]]}) for col in cols]))
    self.save(dict_of_series)

class readOrderproductstrain(TaskParquetDask):
  persist = util.get_cols(order_products_train)
  def run(self):
    file_path = order_products_train
    cols = util.get_cols(file_path)
    # self.persist = cols
    print(cols)
    dict_of_series = dict(zip(cols,[dd.read_csv('../' + file_path,usecols=[col],dtype={col : util.dtypes_convert[util.dtypes[col]]}) for col in cols]))
    self.save(dict_of_series)

class readOrders(TaskParquetDask):
  persist = util.get_cols(orders)
  def run(self):
    file_path = orders
    cols = util.get_cols(file_path)
    # self.persist = cols
    print(cols)
    dict_of_series = dict(zip(cols,[dd.read_csv('../' + file_path,usecols=[col],dtype={col : util.dtypes_convert[util.dtypes[col]]}) for col in cols]))
    self.save(dict_of_series)

class readProducts(TaskParquetDask):
  persist = util.get_cols(products)
  def run(self):
    file_path = products
    cols = util.get_cols(file_path)
    # self.persist = cols
    print(cols)
    dict_of_series = dict(zip(cols,[dd.read_csv('../' + file_path,usecols=[col],dtype={col : util.dtypes_convert[util.dtypes[col]]}) for col in cols]))
    self.save(dict_of_series)

######################################################################################### TRFS
class mergePriorOrders(TaskParquetDask):
  def requires(self):
    return dict(zip(['prior','orders'],[readOrderproductsprior(),readOrders()]))
  def run(self):
    result = util.merge(self.input()['prior'],self.input()['orders'],None,None,'order_id').compute().sort_values(['user_id', 'order_number', 'product_id'], ascending=True)
    self.save(result)

class mergeTrainOrders(TaskParquetDask):
  def requires(self):
    return dict(zip(['train','orders'],[readOrderproductstrain(),readOrders()]))
  def run(self):
    result = util.merge(self.input()['train'],self.input()['orders'],None,['user_id','order_id'],'order_id').compute()
    self.save(result)

class mergeWithTrainTest(TaskParquetDask):
  persist = ['train','test','check']
  def requires(self):
    return dict(zip(['usersFeats','orders','upFeats','products','train'],[usersFeats(),readOrders(),userProductFeats(),productFeats(),mergeTrainOrders()]))
  def run(self):
    TrainTestSelection = util.select(self.input()['orders'],self.input()['orders'],['user_id', 'order_id', 'eval_set', 'days_since_prior_order'],'eval_set',lambda x: x!='prior').set_index('user_id')
    merge_with_users = util.merge(self.input()['usersFeats'],TrainTestSelection,None,None,None,merged=[False,True]).reset_index()# this makes sense because the order file is sorted by user_id and the train and test both contain one order from different groups of users if you get combine both the train and test orders it will make the whole population of users and each user only once . so the indexes match perfectly with the user features table.
    merge_with_products = util.merge(self.input()['upFeats'],self.input()['products'],None,None,'product_id',merged=[False,False])
    data = util.merge(merge_with_products,merge_with_users,None,None,'user_id',merged=[True,True])
    data = util.merge(data,self.input()['train'],None,['user_id','product_id','reordered'],['user_id','product_id'],how='left',merged=[True,True]).compute()
    data['reordered'].fillna(0,inplace=True)
    train = data[data['eval_set'] == 'train'].drop(['eval_set', 'user_id', 'product_id', 'order_id'], axis = 1)
    test =  data[data['eval_set'] == 'test'].drop(['eval_set', 'user_id','reordered'], axis = 1)
    check = data.drop(['eval_set', 'user_id', 'reordered'], axis = 1)
    dct = dict(zip(self.persist,[train,test,check]))
    self.save(dct)

class userProductFeats(TaskParquetDask):
  persist = ['up_orders','up_first_order','up_last_order']
  def requires(self):
    return mergePriorOrders()
  def run(self):
    grouped = util.groupby(self.input(),['user_id','product_id','order_number','add_to_cart_order'],['user_id','product_id'],None,merged=True)
    print(type(grouped))
    up_orders = grouped.size().to_frame('up_orders').reset_index()
    up_first_order = grouped['order_number'].min().to_frame('up_first_order').reset_index(drop=True)
    up_last_order = grouped['order_number'].max().to_frame('up_last_order').reset_index(drop=True)
    # data['up_average_cart_position'] = orders_products.groupby(['user_id', 'product_id'])['add_to_cart_order'].mean()
    dct = dict(zip(self.persist,[up_orders,up_first_order,up_last_order]))
    self.save(dct)

class productFeats(TaskParquetDask): # groupby on product_id
  persist = ['prod_reorder_probability','prod_total','reorders_total','reorder_ratio']
  def requires(self):
    return mergePriorOrders()
  def run(self):
    productTime = util.groupby(self.input(),['user_id','product_id'],['user_id','product_id'],lambda x: x.cumcount().to_frame('productTime'),merged=True) # if you sort this merge first this runs much faster
    selection = util.select(self.input(),productTime,['user_id','product_id'],None,lambda x: x == 0,merged=[True,True])
    selection2 = util.select(self.input(),productTime,['user_id','product_id'],None,lambda x: x == 1,merged=[True,True])
    prod_first_order = util.groupby(selection,None,'product_id',lambda x: x.size().to_frame('prod_first_order'),merged=True)
    prod_second_order = util.groupby(selection2,None,'product_id',lambda x: x.size().to_frame('prod_second_order'),merged=True)
    prod_reorder_probability = (prod_second_order.iloc[:,0]/prod_first_order.iloc[:,0]).to_frame('prod_reorder_probability')
    grouped_prod_id = util.groupby(self.input(),['product_id','reordered'],'product_id',None,merged=True)
    prod_total = grouped_prod_id.size().to_frame('prod_total').astype(np.int32)
    reorders_total = grouped_prod_id.sum().rename(columns={'reordered':'reordersTotal'}).astype(np.float32)
    reorder_ratio = (reorders_total.iloc[:,0] / prod_total.iloc[:,0]).to_frame('reorder_ratio')
    dct = dict(zip(self.persist,[prod_reorder_probability,prod_total,reorders_total,reorder_ratio]))
    self.save(dct)

class usersFeats(TaskParquetDask):
  persist = ['user_orders','user_period','user_mean_days_since_prior','user_reorder_ratio','user_distinct_products','user_total_products']
  def requires(self):
    return dict(zip(['mergePriorOrders','allOrders'],[mergePriorOrders(),readOrders()]))
  def run(self):
    ###################### groupby on user_id on orders file on priors selections
    priors_selection = util.select(self.input()['allOrders'],self.input()['allOrders'],['user_id','order_number','days_since_prior_order'],'eval_set',lambda x: x == 'prior')
    grouped_prio_sel = util.groupby(priors_selection,None,'user_id',None,merged=True)
    user_orders = grouped_prio_sel['order_number'].max().to_frame('user_orders')
    user_period = grouped_prio_sel['days_since_prior_order'].sum().to_frame('user_period')
    user_mean_days_since_prior = grouped_prio_sel['days_since_prior_order'].mean().to_frame('user_mean_days_since_prior')
    ###################### groupby on user_id on mergePriorOrders file (for the products)
    grouped_merged = util.groupby(self.input()['mergePriorOrders'],['user_id','product_id'],'user_id',None,merged=True)
    user_total_products = grouped_merged.size().to_frame('user_total_products')
    user_distinct_products = grouped_merged['product_id'].nunique().to_frame('user_distinct_products')
    reorders_selection = util.select(self.input()['mergePriorOrders'],self.input()['mergePriorOrders'],['user_id','product_id'],'reordered',lambda x: x == 1,merged=[True,True])
    order_number_gt_selection = util.select(self.input()['mergePriorOrders'],self.input()['mergePriorOrders'],['user_id','product_id'],'order_number',lambda x: x > 1,merged=[True,True])
    eq1 = util.groupby(reorders_selection,None,'user_id',lambda x: x.size().to_frame('eq1'),merged=True)
    gt1 = util.groupby(order_number_gt_selection,None,'user_id',lambda x: x.size().to_frame('gt1'),merged=True) # not sure why this works it has less fucking rows
    user_reorder_ratio = (eq1.iloc[:,0] / gt1.iloc[:,0]).to_frame('user_reorder_ratio')
    dct = dict(zip(self.persist,[user_orders,user_period,user_mean_days_since_prior,user_reorder_ratio,user_distinct_products,user_total_products]))
    self.save(dct)

def combi(z,df): # this summarizes the predictions into order_id : list(product_id)
    prd_bag = dict()
    z_bag = dict()
    for row in df.itertuples():
      if row.reordered > z:
        try:
          prd_bag[row.order_id] += ' ' + str(row.product_id)
          z_bag[row.order_id]+= ' ' + str(int(100*row.reordered))
        except:
          prd_bag[row.order_id] = str(row.product_id)
          z_bag[row.order_id]= str(int(100*row.reordered))

    for order in df.order_id:
      if order not in prd_bag:
        prd_bag[order] = ' '
        z_bag[order] = ' '

    return prd_bag,z_bag

# F1 function uses the actual products as a list in the train set and the list of predicted products

def f1_score_single(x):                 #from LiLi but modified to get 1 for both empty

    y_true = x.actual
    y_pred = x.list_prod
    if y_true == '' and y_pred ==[] : return 1.
    y_true = set(y_true)
    y_pred = set(y_pred)
    cross_size = len(y_true & y_pred)
    if cross_size == 0: return 0.
    p = 1. * cross_size / len(y_pred)
    r = 1. * cross_size / len(y_true)
    return 2 * p * r / (p + r)

######################################################################################### VIZ

class viz(d6tflow.tasks.TaskData):
  def requires(self):
    # return dict(zip(util.dsts,[util.readTrainCols(),util.readTestCols()]))
    return dict(zip(['prior','orders','products','departments'],[readOrderproductsprior(),readOrders(),readProducts(),readDepartments()]))
    # return readOrders()

  def run(self):
    ######################## orders

    # unlabelenc = dict(zip(list(range(7)),['Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri']))
    # unlabeled = util.applymapOnColumns([ self.input()['order_dow'].load() ],['unlabeled'],lambda x: unlabelenc[x])
    # viz_util.freq(unlabeled)

    # viz_util.freq(self.input()['order_hour_of_day'].load())

    # agged = util.agg(self.input()['days_since_prior_order'].load(),self.input()['order_id'].load(),['count'])
    # viz_util.bar(agged)

    # print(self.input()['user_id'].load()['user_id'].compute().nunique())

    # agged = util.agg(self.input()['user_id'].load(),self.input()['days_since_prior_order'].load(),['mean'])
    # viz_util.hist(agged,30,0,33)

    ######################## merge

    # grpd = util.groupby(self.input()['orders'],['user_id','order_number'],'user_id',lambda x: x['order_number'].max().value_counts().to_frame())
    # viz_util.bar(grpd,'order_number',merged=True)

    products = util.merge(self.input()['prior'],self.input()['products'],'product_id',['product_id','department_id'],'product_id')
    products = util.merge(products,self.input()['departments'],None,None,'department_id',merged=[True,False])
    # basic_info_df(products)
    viz_util.pie(products,'department',merged=True)


######################################################################################### JOINS AND TRAINING


class TrainEval(TaskParquetDask):
  persist = ['X_train', 'X_eval', 'y_train', 'y_eval']
  def requires(self):
    return mergeWithTrainTest()
  def run(self):
    train = self.input()['train'].load()
    X_train, X_eval, y_train, y_eval = train_test_split(train[train.columns.difference(['reordered'])], train['reordered'], test_size=0.1, random_state=2)
    dct = dict(zip(self.persist,[X_train, X_eval, y_train.to_frame('y_train'), y_eval.to_frame('y_eval')]))
    self.save(dct)
    # viz_util.sanity_check(self.input(),'test')

class Lgb(d6tflow.tasks.TaskPickle):
  def requires(self):
    return TrainEval()
  def run(self):
    print('formatting and training LightGBM ...')
    X_train = self.input()['X_train'].load(pd=True)
    X_eval= self.input()['X_eval'].load(pd=True)
    y_train= self.input()['y_train'].load(pd=True).iloc[:,0]
    y_eval  = self.input()['y_eval'].load(pd=True).iloc[:,0]
    print(list(map(type,[X_train, X_eval, y_train, y_eval])))
    lgb_train = lgb.Dataset(X_train, label=y_train)
    lgb_eval = lgb.Dataset(X_eval, y_eval, reference = lgb_train)
    params = {'task': 'train', 'boosting_type': 'gbdt',   'objective': 'binary', 'metric': {'binary_logloss', 'auc'},
    'num_iterations' : 1000, 'max_bin' : 100, 'num_leaves': 512, 'feature_fraction': 0.8,  'bagging_fraction': 0.95,
    'bagging_freq': 5, 'min_data_in_leaf' : 200, 'learning_rate' : 0.05}
    # set lower num_boost_round (I used 300 instead of 50 at home) to avoid time-out on Kaggle
    lgb_model = lgb.train(params, lgb_train, num_boost_round = 50, valid_sets = lgb_eval, early_stopping_rounds=10)
    self.save(lgb_model)


class f1Calc(d6tflow.tasks.TaskPickle):
  def requires(self):
    return dict(zip(['actual','trainTestMerge','checkReorder'],[trainTestActual(),mergeWithTrainTest(),checkReorder()]))
  def run(self):
    print(' summarizing products and probabilities ...')
    # get the prediction for a range of thresholds
    tt=self.input()['actual']['traintest'].load()
    n_actual = self.input()['actual']['n_actual'].load()
    print(type(tt))
    checkReorder = self.input()['checkReorder'].load()
    check = self.input()['trainTestMerge']['check'].load(pd=True)
    check['reordered'] = checkReorder
    print(type(check))
    i=0
    for z in [0.17, 0.21, 0.25]:
        prd_bag,z_bag = combi(z,check)
        ptemp = pd.DataFrame.from_dict(prd_bag, orient='index')
        ptemp.reset_index(inplace=True)
        ztemp = pd.DataFrame.from_dict(z_bag, orient='index')
        ztemp.reset_index(inplace=True)
        ptemp.columns = ['order_id', 'products']
        ztemp.columns = ['order_id', 'zs']
        ptemp['list_prod'] = ptemp['products'].apply(lambda x: list(map(int, x.split())))
        ztemp['list_z'] = ztemp['zs'].apply(lambda x: list(map(int, x.split())))
        n_cart = ptemp['products'].apply(lambda x: len(x.split())).mean()
        tt = tt.merge(ptemp,on='order_id',how='inner')
        tt = tt.merge(ztemp,on='order_id',how='inner')
        tt.drop(['products','zs'],axis=1,inplace=True)
        tt['zavg'] = tt['list_z'].apply(lambda x: 0.01*np.mean(x) if x!=[] else 0.).astype(np.float16)
        tt['zmax'] = tt['list_z'].apply(lambda x: 0.01*np.max(x) if x!=[] else 0.).astype(np.float16)
        tt['zmin'] = tt['list_z'].apply(lambda x: 0.01*np.min(x) if x!=[] else 0.).astype(np.float16)
        tt['f1']=tt.apply(f1_score_single,axis=1).astype(np.float16)
        F1 = tt['f1'].loc[tt['eval_set']==1].mean()
        tt = tt.rename(columns={'list_prod': 'prod'+str(i), 'f1': 'f1'+str(i), 'list_z': 'z'+str(i),
                    'zavg': 'zavg'+str(i), 'zmax': 'zmax'+str(i),  'zmin': 'zmin'+str(i)})
        print(' z,F1,n_actual,n_cart :  ', z,F1,n_actual,n_cart)
        i=i+1
    tt['fm'] = tt[['f10', 'f11', 'f12']].idxmax(axis=1)
    tt['f1'] = tt[['f10', 'f11', 'f12']].max(axis=1)
    tt['fm'] = tt.fm.replace({'f10': 0,'f11': 1, 'f12':2}).astype(np.uint8)
    print(' f1 maximized ', tt['f1'].loc[tt['eval_set']==1].mean())
    self.save(tt)


class trainTestActual(d6tflow.tasks.TaskPickle): # Training and test data for later use in F1 optimization and training
  persist = ['traintest','n_actual']
  def requires(self):
    return dict(zip(['train','orders'],[mergeTrainOrders(),readOrders()]))
  def run(self):
    train_orders = util.select(self.input()['train'],self.input()['train'],None,'reordered',lambda x: x == 1,merged=[True,True],pd=True).drop('reordered',axis=1)
    orders = df_concat(self.input()['orders'],None,with_pd=True)
    orders = orders.set_index('order_id')
    train=orders[['eval_set']].loc[orders['eval_set']=='train']
    train['actual'] = train_orders.groupby('order_id').agg({'product_id':lambda x: list(x)})
    train['actual']=train['actual'].fillna('')
    # basic_info_df(train,pd=True)
    n_actual = train['actual'].apply(lambda x: len(x)).mean()   # this is the average cart size
    test=orders[['eval_set']].loc[orders['eval_set']=='test']
    test['actual']=' '
    # basic_info_df(test,pd=True)
    traintest=pd.concat([train,test])
    # basic_info_df(traintest,pd=True)
    dct = dict(zip(self.persist,[traintest,n_actual]))
    self.save(dct)


class checkReorder(d6tflow.tasks.TaskPickle): # predictin col (reordered) for check (train + test)
  def requires(self):
    return dict(zip(['merge','model'],[mergeWithTrainTest(),Lgb()]))
  def run(self):
    check = self.input()['merge']['check'].load(pd=True)
    lgb_model = self.input()['model'].load()
    check_pred = lgb_model.predict(check[check.columns.difference(['order_id', 'product_id'])], num_iteration = lgb_model.best_iteration)
    print(type(check_pred))
    self.save(check_pred)

class secondClassifier(d6tflow.tasks.TaskCSVPandas): # Fitting the second classifier for F1 ...
  def requires(self):
    return f1Calc()
  def run(self):
    tt = self.input().load()
    print(type(tt))
    X=tt[[ 'zavg0', 'zmax0','zmin0', 'zavg1', 'zmax1', 'zmin1', 'zavg2', 'zmax2', 'zmin2']].loc[tt['eval_set']=='train']
    y=tt['fm'].loc[tt['eval_set']=='train']
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1)

    clf = GradientBoostingClassifier().fit(X_train, y_train)
    print('GB Accuracy on training set: {:.2f}' .format(clf.score(X_train, y_train)))
    print('Accuracy on test set: {:.2f}' .format(clf.score(X_test, y_test)))
    #pd.DataFrame(clf.feature_importances_, index=X_train.columns, columns=["Importance"]).plot(kind='bar')
    #plt.show()

    final=tt[['order_id','prod0','prod1','prod2','zavg0']].loc[tt['eval_set']=='test']
    df_test=tt[[ 'zavg0', 'zmax0','zmin0', 'zavg1', 'zmax1', 'zmin1', 'zavg2', 'zmax2', 'zmin2']].loc[tt['eval_set']=='test']
    final['fit']= clf.predict(df_test)
    final['best'] = final.apply(lambda row: row['prod0'] if row['fit']==0 else
                                     ( row['prod1'] if row['fit']==1 else  row['prod2'] )  , axis=1)
    #final['products']=final['best'].apply(lambda x: ' '.join(str(i) for i in x) if x!=[] else 'None')

    # I am adding 'None' to orders with one or two products because of the bias in F1
    final['products']=final.apply(self.mylist,axis=1)

    self.save(final[['order_id','products']],index=False)

  def mylist(self,x):
    prodids = x.best
    zavg = x.zavg0
    if prodids == []: return 'None'
    if zavg < 0.5:
        if len(prodids) == 1: return  str(prodids[0])+' None'
        if len(prodids) == 2: return  str(prodids[0])+ ' '+ str(prodids[1]) +' None'
    return ' '.join(str(i) for i in prodids)


class featureImp(TaskParquetDask):
  def requires(self):
    return Lgb()
  def run(self):
    lgb.plot_importance(self.input().load(), figsize=(7,9))
    plt.show()


if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('-r', action='store_true')
  parser.add_argument('-b', action='store_true')
  parser.add_argument('-e', action='store_true')
  parser.add_argument('-c', action='store_true')
  parser.add_argument('-s', action='store_true')
  parser.add_argument('-t', action='store')
  parser.add_argument('-i', help='delimited list input', type=str)
  args = parser.parse_args()

  start_time = time.time()

  if args.c:
    list(map(lambda x: print(x[0]),inspect.getmembers(sys.modules[__name__], lambda member: inspect.isclass(member) and member.__module__ == __name__)))
  else:
    task = locals()[args.t]
    d6tflow.preview(task())
    if args.s:
      d6tflow.run(viz_util.see_outputs(task))
    elif args.r:
      task().run()
    elif args.i:
      html = True if args.b else False
      if args.i == 'all':
        d6tflow.run(basic_info(task,html=html))
      else:
        my_list = [str(item) for item in args.i.split(',')]
        d6tflow.run(basic_info(task,cols=my_list,html=html))
    elif args.b:
      d6tflow.run(task())
    elif args.e:
      d6tflow.run(extensive_info(task))
    else:
      d6tflow.run(task())
    # task().invalidate(confirm=False)

  elapsed_time = time.time() - start_time
  print("Elapsed time: {}".format(util.hms_string(elapsed_time)))


