from dask.delayed import delayed ,Delayed
from dask.distributed import Client
import dask.dataframe as dd
import dask
import d6tflow
import pandas as pd
import gc

class ParquetTargetDask(d6tflow.targets.DataTarget):
  def load(self, cached=False, **kwargs):
    if 'dask' in kwargs and kwargs['dask']:
      print('loading with dask')
      if 'cols' in kwargs and kwargs['cols']:
        return super().load(lambda x: dd.read_parquet(x,columns=kwargs['cols']), cached)
      else:
        return super().load(lambda x: dd.read_parquet(x), cached)
    print('loading with pandas')
    if 'cols' in kwargs and kwargs['cols']:
      return super().load(lambda x: pd.read_parquet(x,columns=kwargs['cols']), cached)
    else:
      return super().load(lambda x: pd.read_parquet(x), cached)

  def save(self, obj, **kwargs):
    (self.path).parent.mkdir(parents=True, exist_ok=True)
    if isinstance(obj,pd.Series) or isinstance(obj,pd.DataFrame):
      print('saving pd obj')
      obj.to_parquet(self.path, engine='fastparquet')
    else:
      print('saving dd obj')
      client = Client()
      # print(client)
      client.compute(dd.to_parquet(obj,self.path, engine='fastparquet'))
      del obj
      client.close()
    gc.collect()
    return self.path

class TaskParquetDask(d6tflow.tasks.TaskData):
  target_class = ParquetTargetDask
  target_ext = 'parquet'

class pltTarget(d6tflow.targets._LocalPathTarget):
  """
  Saves to png. Does not load
  """
  def load(self):
    raise RuntimeError('Images can only be saved not loaded')
  def save(self, obj, **kwargs):
    """
    Save obj to pickle and png

    Args:
      obj (obj): python object
      plotkwargs (dict): additional arguments to plt.savefig()
      kwargs : additional arguments to pass to pickle.dump

    Returns: filename

    """
    # fig = obj.get_figure()
    obj.savefig(self.path, **kwargs)
    obj.show()
    return self.path

class Taskplt(d6tflow.tasks.TaskData):
  """
  Task which saves plots to png, does not load
  """
  target_class = pltTarget
  target_ext = 'png'
